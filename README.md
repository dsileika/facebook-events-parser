# Local events

Facebook events by user GPS location

## Getting Started

This project is done in Kotlin with respectful to Clean Architecture

### Prerequisites

Kotlin

![events-1](events-1.png)
![events-2](events-2.png)

Demo
[![google-play](en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=drama.code.localevents)

### Installing

It just needs to import to Android Studio 3(Gradle 4) as a Kotlin project

## Built With

* [The Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) - The Architecture
* [Dagger2](https://maven.apache.org/) - Dependency Management
* [Kotlin](https://kotlinlang.org/) - Statically typed programming language
* [Rxjava](https://github.com/ReactiveX/RxJava/) - A library for composing asynchronous and event-based programs
* [Retrofit2](http://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java
* [Glide](https://github.com/bumptech/glide) - An image loading and caching library for Android focused on smooth scrolling
* [ConstraintLayout](https://developer.android.com/training/constraint-layout/index.html) -A Responsive UI layout manager

## Backend
* [Facebook Events by Location](https://github.com/tobilg/facebook-events-by-location) - Parsing Facebook events
 
## Versioning

TBA
 
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
Kotlin
