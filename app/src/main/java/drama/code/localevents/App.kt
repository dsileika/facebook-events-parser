package drama.code.localevents

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import dagger.Provides
import drama.code.localevents.components.AppComponent
import drama.code.localevents.components.DaggerAppComponent
import drama.code.localevents.event.module.EventsModule
import drama.code.localevents.event.view.EventsFragment
import drama.code.localevents.modules.AppModule
import drama.code.localevents.modules.NetworkModule
import retrofit2.http.POST
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


/**
 * Created by localevents on 4/11/17.
 */
class App: Application() {

    companion object {

        // User/App Acess token key, get token: https://developers.facebook.com/tools/accesstoken/
        var fb_token: String? = null

        // API call timeout in milliseconds
        var timeout_in_ms: Int = 30000

        fun get(context: Context): App {
            return context.applicationContext as App
        }

        fun returnDataString(isoString: String, position: Int) : String {

            // 2017-09-11T01:16:13.858Z - ISO
            // 2018-01-05T22:30:00+0100 - FB ISO
//        val isoFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
                    val isoFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault())
                    isoFormatter.timeZone = TimeZone.getTimeZone("UTC")
            var convertedDate: Date
            try {
                        convertedDate = isoFormatter.parse(isoString)
                    } catch (e: ParseException) {
                        return ""
//                        Log.d("Parse", "Cannot parse date")
                    } catch (e: KotlinNullPointerException){
                        return ""
                    }

                    val outDateString = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())

                    var qStr = outDateString.format(convertedDate)
                    if (position == 1) qStr = " - " + qStr
                    return qStr

        }

        fun openInMap(context: Context, latitude: Double, longitude: Double, mTitle: String){
//            val uri = String.format(Locale.getDefault(), "geo:%f,%f", latitude, longitude)
            val geoUri = "http://maps.google.com/maps?q=loc:$latitude,$longitude ($mTitle)"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
            return context.startActivity(intent)
        }

        /**
         * Get the facebook intent for the given facebook
         * profile id. If the facebook app is installed, then
         * it will open the facebook app. Otherwise, it will
         * open the facebook profile page in browser.
         *
         * @return - the facebook intent
         */
        fun getFBIntent(context: Context, facebookId: String): Intent {

            try {
                // Check if FB app is even installed
                context.packageManager.getPackageInfo("com.facebook.katana", 0);

                val facebookScheme: String = "fb://event/" + facebookId;
                return Intent(Intent.ACTION_VIEW, Uri.parse(facebookScheme));
            }
            catch(e: Exception) {

                // Cache and Open a url in browser
                val facebookProfileUri: String  = "https://www.facebook.com/events/" + facebookId;
                return Intent(Intent.ACTION_VIEW, Uri.parse(facebookProfileUri));
            }

        }

        fun toCalendar(year: Int?, monthOfYear: Int?, dayOfMonth: Int?, hourOfDay: Int?, minute: Int?): Calendar {
            val cal = Calendar.getInstance()

            if(year != null && monthOfYear != null && dayOfMonth != null && hourOfDay == null && minute == null) cal.set(year, monthOfYear, dayOfMonth) // set Month field to Zero : Only Date
            if(year == null && monthOfYear == null && dayOfMonth == null && hourOfDay != null && minute != null) cal.set(0, 0, 0, hourOfDay, minute) // set Month field to Zero : Only Time
            if(year != null && monthOfYear != null && dayOfMonth != null && hourOfDay != null && minute != null) cal.set(year, monthOfYear, dayOfMonth, hourOfDay, minute) // set Month field to Zero : Both

            val date: Date = cal.time
            cal.time = date
            return cal
        }

        @SuppressLint("SimpleDateFormat")
        fun compareDates(date1: Calendar, date2: Calendar, strName: String = ""): Int {

            // Date1
            val dayDate1Cal: Int = date1.get(Calendar.DAY_OF_MONTH)
            val monthDate1Cal: Int = date1.get(Calendar.MONTH)
            val yearDate1Cal: Int = date1.get(Calendar.YEAR)
            val hourDate1Cal: Int = date1.get(Calendar.HOUR_OF_DAY)
            val minuteDate1Cal: Int = date1.get(Calendar.MINUTE)

            val yearDate1Str: String = yearDate1Cal.toString()
            val monthDate1Str: String = if(monthDate1Cal < 10) "0" + monthDate1Cal.toString() else "" + monthDate1Cal.toString()
            val dayDate1Str: String = if(dayDate1Cal < 10) "0" + dayDate1Cal.toString() else "" + dayDate1Cal.toString()
            val hourDate1Str: String = if(hourDate1Cal < 10) "0" + hourDate1Cal.toString() else "" + hourDate1Cal.toString()
            val minuteDate1Str: String = if(minuteDate1Cal < 10) "0" + minuteDate1Cal.toString() else "" + minuteDate1Cal.toString()


            // Date2
            val dayDate2Cal: Int = date2.get(Calendar.DAY_OF_MONTH)
            val monthDate2Cal: Int = date2.get(Calendar.MONTH)
            val yearDate2Cal: Int = date2.get(Calendar.YEAR)
            val hourDate2Cal: Int = date2.get(Calendar.HOUR_OF_DAY)
            val minuteDate2Cal: Int = date2.get(Calendar.MINUTE)

            val yearDate2Str: String = yearDate2Cal.toString()
            val monthDate2Str: String = if(monthDate2Cal < 10) "0$monthDate2Cal" else "" + monthDate2Cal.toString()
            val dayDate2Str: String = if(dayDate2Cal < 10) "0$dayDate2Cal" else "" + dayDate2Cal.toString()
            val hourDate2Str: String = if(hourDate2Cal < 10) "0$hourDate2Cal" else "" + hourDate2Cal.toString()
            val minuteDate2Str: String = if(minuteDate2Cal < 10) "0$minuteDate2Cal" else "" + minuteDate2Cal.toString()

            var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm")
            var date1 = sdf.parse("$yearDate1Str-$monthDate1Str-$dayDate1Str $hourDate1Str:$minuteDate1Str")
            var date2 = sdf.parse("$yearDate2Str-$monthDate2Str-$dayDate2Str $hourDate2Str:$minuteDate2Str")

            when(strName){
                "date" -> {
                    sdf = SimpleDateFormat("yyyy-MM-dd")
                    date1 = sdf.parse("$yearDate1Str-$monthDate1Str-$dayDate1Str")
                    date2 = sdf.parse("$yearDate2Str-$monthDate2Str-$dayDate2Str")
                }
                "time" -> {
                    sdf = SimpleDateFormat("HH:mm")
                    date1 = sdf.parse("$hourDate1Str:$minuteDate1Str")
                    date2 = sdf.parse("$hourDate2Str:$minuteDate2Str")
                }
            }

            println("date1 : " + sdf.format(date1))
            println("date2 : " + sdf.format(date2))

            if (date1 > date2) return 1  // Date1 is after Date2
            if (date1 < date2) return  2 // Date1 is before Date2
            if (date1 == date2) return 3 // Date1 is equal to Date2
            if (date1 >= date2) return 4 // Date1 is after or equal to Date2
            if (date1 <= date2) return 5 // Date1 is before or equal to Date2

            return 0
        }
     }

    val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(resources.getString(R.string.base_url)))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}