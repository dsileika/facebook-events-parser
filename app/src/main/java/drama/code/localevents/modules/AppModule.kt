package drama.code.localevents.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import drama.code.localevents.App
import javax.inject.Singleton

/**
 * Created by localevents on 4/11/17.
 */
@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }
}