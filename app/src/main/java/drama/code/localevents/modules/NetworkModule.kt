package drama.code.localevents.modules

import android.content.Context
import com.google.gson.GsonBuilder
import drama.code.localevents.event.model.Events

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import com.google.gson.Gson
import drama.code.localevents.Utils.CachingInterceptor
import drama.code.localevents.Utils.DeserializerJsonEvents
import okhttp3.Cache
import java.io.File


/**
 * Created by localevents on 4/11/17.
 */

@Module
class NetworkModule(private val base_url: String) {

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

/*    @Provides
    @Singleton
    fun provideApiInterceptor(): ApiInterceptor {
        return ApiInterceptor()
    }*/

    @Provides
    @Singleton
    fun provideCachingInterceptor(): CachingInterceptor {
        return CachingInterceptor()
    }



    @Provides
    @Singleton
    fun provideHttpClient(loggingInterceptor: HttpLoggingInterceptor, cache: okhttp3.Cache, cachingInterceptor: CachingInterceptor): OkHttpClient {

        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .retryOnConnectionFailure(true)
                .addInterceptor(cachingInterceptor)
                .connectTimeout(3000, TimeUnit.MILLISECONDS)
                .cache(cache)
                .build()
    }

   @Provides
    @Singleton
    fun provideCache(context: Context): Cache {

        val cacheSize: Long = 5 * 1024 * 1024
        var cacheDir: File = context.cacheDir
        return Cache(cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    fun provideRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val builder = GsonBuilder()
        builder.registerTypeAdapter(Events::class.java, DeserializerJsonEvents<Events>())
        return builder.create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, rxAdaptor: RxJava2CallAdapterFactory, gson: Gson) : Retrofit {

            return Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create(gson))//mapper
                    .client(okHttpClient)
                    .addCallAdapterFactory(rxAdaptor)
                    .build()
    }
}