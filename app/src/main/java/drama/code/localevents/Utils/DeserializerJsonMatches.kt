package drama.code.localevents.Utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import com.google.gson.Gson
import drama.code.localevents.event.model.Event
import io.reactivex.Observable
import drama.code.localevents.event.model.Events
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by localevents on 8/11/17.
 */
class DeserializerJsonEvents<T> : JsonDeserializer<Events> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Events {

        val events = Events()

                Observable.just(json!!.asJsonObject.get("events").toString())
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { elements ->
                            Gson().fromJson(elements, arrayOf(Event())::class.java)
                        }
                        .flatMap { jsonObjects -> Observable.fromArray(jsonObjects)
                        }
                        .blockingSubscribe { listOfEvents -> events.setEvents(listOfEvents.toList())
                        }
//                        .blockingSubscribe({
//                            listOfEvents -> events.setEvents(listOfEvents.toList())
//                        })

        return events
    }
}