package drama.code.localevents.Utils

import java.util.*




/**
 * Created by dofke on 1/26/18.
 */
class CalUtil {
    fun day(day: Int,setcal: Calendar): Calendar {
        val calendar = Calendar.getInstance()
        calendar.time = setcal.time
        calendar.add(Calendar.DATE, day)
        return calendar
    }

    fun hour(hour: Int,endTime: Calendar, startTime: Calendar): Calendar {
        val calendar = Calendar.getInstance()
//        calendar.time = endTime.time
        calendar.set(endTime.get(Calendar.YEAR),endTime.get(Calendar.MONTH),endTime.get(Calendar.DAY_OF_MONTH),startTime.get(Calendar.HOUR_OF_DAY),startTime.get(Calendar.MINUTE))
        calendar.add(Calendar.HOUR_OF_DAY, hour)
        return calendar
    }
}