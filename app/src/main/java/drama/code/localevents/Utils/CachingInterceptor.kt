package drama.code.localevents.Utils

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.util.concurrent.TimeUnit


/**
 * Created by dofke on 1/26/18.
 */
class CachingInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        request = Request.Builder()
                .cacheControl(CacheControl.Builder()
                        .maxAge(0, TimeUnit.DAYS)
                        .minFresh(0, TimeUnit.HOURS)
                        .maxStale(0, TimeUnit.HOURS)
                        .build())
                .url(request.url())
                .build()


        return chain.proceed(request)
    }
}