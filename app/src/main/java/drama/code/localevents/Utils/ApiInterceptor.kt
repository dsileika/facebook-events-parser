package drama.code.localevents.Utils

import drama.code.localevents.App
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by localevents on 5/11/17.
 */
class ApiInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder().
//                addQueryParameter("api_key", API.KEY.value).
//                addQueryParameter("lat", App.lat.toString()).
//                addQueryParameter("lng", App.lng.toString()).
//                addQueryParameter("accessToken", App.fb_token).
//                addQueryParameter("sort", "time").
//                addQueryParameter("since", (System.currentTimeMillis()/1000).toString()).
//                addQueryParameter("categories", App.categories).
                build()

        val requestBuilder = original.newBuilder().url(url)
        val  request = requestBuilder.build()

        return chain.proceed(request)
    }
}

