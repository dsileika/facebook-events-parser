package drama.code.localevents.Utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.NetworkOnMainThreadException
import drama.code.localevents.App
import drama.code.localevents.R
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


/**
 * Created by dofke on 2018-01-06.
 */
object NetworkUtil {
    var TYPE_WIFI = 1
    var TYPE_MOBILE = 2
    var TYPE_NOT_CONNECTED = 0
    val NETWORK_STATUS_NOT_CONNECTED = 0
    val NETWORK_STAUS_WIFI = 1
    val NETWORK_STATUS_MOBILE = 2

    fun getConnectivityStatus(context: Context): Int {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        if (null != activeNetwork) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI

            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE
        }
        return TYPE_NOT_CONNECTED
    }

    fun getConnectivityStatusString(context: Context): Int {
        val conn = NetworkUtil.getConnectivityStatus(context)
        var status = 0
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = NETWORK_STAUS_WIFI
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = NETWORK_STATUS_MOBILE
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED
        }
        return status
    }

    @SuppressLint("LongLogTag")
    fun hasActiveInternetConnection(context: Context): Boolean {
        if (getConnectivityStatusString(context) > 0) {
            try {
                val urlc = URL(context.getString(R.string.base_url)+context.getString(R.string.health_prefix)).openConnection() as HttpURLConnection
                urlc.setRequestProperty("User-Agent", "Test")
                urlc.setRequestProperty("Connection", "close")
                urlc.connectTimeout = 5500
                urlc.connect()
//                Log.d("CODE", urlc.responseCode.toString())
                return urlc.responseCode == 200
//                return true
            } catch (e: IOException) {
                return false
//                Log.e("IOException", "Error checking internet connection", e)
            } catch (e: NetworkOnMainThreadException){
                return false
//                Log.e("NetworkOnMainThreadException", "Error checking internet connection", e)
            }

        } else {
            return false
//            Log.d("NetworkStats", "No network available!")
        }
    }
}