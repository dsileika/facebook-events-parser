package drama.code.localevents.event.view

import android.annotation.SuppressLint
import android.os.Bundle
import com.klinker.android.sliding.SlidingActivity
import drama.code.localevents.R
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.opengl.Visibility
import com.bumptech.glide.Glide
import android.view.MenuItem
import android.view.View
import com.klinker.android.sliding.MultiShrinkScroller
import drama.code.localevents.App
import drama.code.localevents.Utils.LinkUtils
import kotlinx.android.synthetic.main.activity_content.*


/**
 * Created by dofke on 2018-01-16.
 */
class EventDetailActivity : SlidingActivity() {

    var id: String? = null
    var image_url: String? = null
    var event_name: String? = null
    var name: String? = null
    var description: String? = null
    var street: String? = null
    var lat: Double? = null
    var lng: Double? = null

    var start_time: String? = null
    var end_time: String? = null

    var event_distance: Int? = null
    /**
     * Initialize our values, this is overridden instead of onCreate as it should be in all
     * sliding activities.
     * @param savedInstanceState the saved state.
     */
    @SuppressLint("SetTextI18n")
    override fun init(savedInstanceState: Bundle?) {
//        setActionBar(findViewById(R.id.tToolbar))

        setContent(R.layout.activity_content)

        enableFullscreen()

        setPrimaryColors(
                resources.getColor(R.color.colorPrimary),
                resources.getColor(R.color.colorPrimaryDark)
        )

        name = intent.extras.getString("name")
        if(name != null) {
            title = name
        }
        event_name = intent.extras.getString("event_name")

        if(event_name != null) {
            person_name.text = event_name
        }

        image_url = intent.extras.getString("event_image")
        // no need to set a color here, palette will generate colors for us to be set
            if(image_url != null) {

                Glide.with(this)
                        .load(image_url)
                        .placeholder(R.drawable.headshot_blank_large)
                        .error(R.drawable.headshot_blank_large)
                        .into(findViewById(R.id.photo))

            } else {

            }

        description = intent.extras.getString("event_description")

        if(description != null) {

            person_age.text = description

            LinkUtils.autoLink(person_age, object : LinkUtils.OnClickListener {
                override fun onLinkClicked(link: String?) {
                }
                override fun onClicked() {
                }
            })
        }

        street = intent.extras.getString("street")

        lat = intent.extras.getDouble("lat")
        lng = intent.extras.getDouble("lng")

        if(lat != null && lng != null && street != null) {
            locationinmap.visibility = View.VISIBLE
            locationinmap?.text = street
            locationinmap.setOnClickListener {
                    App.openInMap(this, lat!!, lng!!, street!!)
            }
        } else {
            locationinmap.visibility = View.GONE
        }

        start_time = intent.extras.getString("start_time")
        end_time = intent.extras.getString("end_time")

        if(!start_time.isNullOrEmpty()) {
            eventtime.visibility = View.VISIBLE
           if(!end_time.isNullOrEmpty()) eventtime.text = start_time + end_time
           if(end_time.isNullOrEmpty()) eventtime.text = start_time
        } else {
            eventtime.visibility = View.GONE
        }

        event_distance = intent.extras.getInt("event_distance")

        if(event_distance != null){
            eventdistance.visibility = View.VISIBLE
            eventdistance.text = if(event_distance!! < 1000) event_distance.toString() +" "+ resources.getString(R.string.meters) else (event_distance!! * 0.001).toString() +" "+ resources.getString(R.string.kilometers)
        } else {
            eventdistance.visibility = View.GONE
        }

        id = intent.extras.getString("event_id")


        if(!id.isNullOrEmpty()) {
            fbEvent.visibility = View.VISIBLE
            fbEvent.setOnClickListener {
                val intent: Intent = App.getFBIntent(this, id!!)
                startActivity(intent)
            }
        } else {
            fbEvent.visibility = View.GONE
        }

        // if we wanted to set some manually instead, do this after setting the image
        // setPrimaryColors(
        //         getResources().getColor(R.color.image_activity_primary),
        //         getResources().getColor(R.color.image_activity_primary_dark)
        // );

        // if we want the image to animate in, then set it after the activity has been created
        // NOTE: this will not change the activity's colors using palette, so make sure you call
        //       setPrimaryColors() first
        // new Handler().postDelayed(new Runnable() {
        //     @Override
        //     public void run() {
        //         setImage(R.drawable.profile_picture);
        //     }
        // }, 500);

        /*   val intent = intent
           if (intent.getBooleanExtra(SampleActivity.ARG_USE_EXPANSION, false)) {
               expandFromPoints(
                       intent.getIntExtra(SampleActivity.ARG_EXPANSION_LEFT_OFFSET, 0),
                       intent.getIntExtra(SampleActivity.ARG_EXPANSION_TOP_OFFSET, 0),
                       intent.getIntExtra(SampleActivity.ARG_EXPANSION_VIEW_WIDTH, 0),
                       intent.getIntExtra(SampleActivity.ARG_EXPANSION_VIEW_HEIGHT, 0)
               )
           }*/
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === android.R.id.home) {
            onBackPressed()
            return true
        }
        // other menu select events may be present here

        return super.onOptionsItemSelected(item)
    }



    override fun configureScroller(scroller: MultiShrinkScroller) {
        super.configureScroller(scroller)
        scroller.intermediateHeaderHeightRatio = 1f
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        // getIntent() should always return the most recent
        setIntent(intent)
    }

}