package drama.code.localevents.event.view

/**
 * Created by dofke on 2018-01-17.
 */
import android.app.DialogFragment
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog

import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class TimePickerFragment : DialogFragment() {

    private var onTimeSetListener: TimePickerDialog.OnTimeSetListener? = null
    private var tpd: TimePickerDialog? = null
    var mDate: Calendar? = null
    var minDate: Calendar = Calendar.getInstance()
    var isEquals: Boolean = false

    fun newInstance(minDate: Calendar, onTimeSetListener: TimePickerDialog.OnTimeSetListener, mTimer: Calendar?, isEquals: Boolean = false): TimePickerFragment {
        val pickerFragment = TimePickerFragment()
        pickerFragment.setOnTimeSetListener(onTimeSetListener)
        pickerFragment.mDate = mTimer ?: Calendar.getInstance()
        pickerFragment.minDate = minDate
        pickerFragment.isEquals = isEquals
        //Pass the date in a bundle.
        val bundle = Bundle()
//        bundle.putSerializable(MOVE_IN_DATE_KEY, date)
//        pickerFragment.arguments = bundle
        return pickerFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        super.onCreateView(inflater, container, savedInstanceState)
        /*
                It is recommended to always create a new instance whenever you need to show a Dialog.
                The sample app is reusing them because it is useful when looking for regressions
                during testing
                 */
        if (tpd == null) {
            tpd = TimePickerDialog.newInstance(
                    onTimeSetListener,
                    mDate!!.get(Calendar.HOUR_OF_DAY), mDate!!.get(Calendar.MINUTE), true
            )
        } else {
            tpd!!.initialize(
                    onTimeSetListener,
                    mDate!!.get(Calendar.HOUR_OF_DAY), mDate!!.get(Calendar.MINUTE), mDate!!.get(Calendar.SECOND), true
            )
        }

        tpd!!.setOnDismissListener { this.dismiss() }

        if(isEquals) tpd!!.setMinTime(minDate.get(Calendar.HOUR_OF_DAY), minDate.get(Calendar.MINUTE), minDate.get(Calendar.SECOND))
//        tpd!!.setOnCancelListener({ Log.d("TimePicker", "Dialog was cancelled") })

        tpd!!.show(fragmentManager, "Timepickerdialog")

        return view
    }


    private fun setOnTimeSetListener(listener: TimePickerDialog.OnTimeSetListener) {
        this.onTimeSetListener = listener
    }

    private fun ymdTripleFor(date: Date): IntArray {
        val cal = Calendar.getInstance(Locale.getDefault())
        cal.time = date
        return intArrayOf(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
    }

    /*

    private var tpd: TimePickerDialog? = null
    var onTimeSet: TimePickerDialog.OnTimeSetListener? = null

    fun newInstance(): TimePickerFragment {
        val frag = TimePickerFragment()
        return frag
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        super.onCreateView(inflater, container, savedInstanceState)

        val now = Calendar.getInstance()
        /*
                It is recommended to always create a new instance whenever you need to show a Dialog.
                The sample app is reusing them because it is useful when looking for regressions
                during testing
                 */
        if (tpd == null) {
            tpd = TimePickerDialog.newInstance(
                    this@TimePickerFragment,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    true
            )
        } else {
            tpd!!.initialize(
                    this@TimePickerFragment,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    now.get(Calendar.SECOND),
                    true
            )
        }

        tpd!!.setOnCancelListener({ Log.d("TimePicker", "Dialog was cancelled") })

        tpd!!.show(fragmentManager, "Timepickerdialog")

        return view
    }

    fun setCallBack(onTime: TimePickerDialog.OnTimeSetListener) {
        onTimeSet = onTime
    }

    override fun onTimeSet(view: TimePickerDialog, hourOfDay: Int, minute: Int, second: Int) {
        val hourString = if (hourOfDay < 10) "0" + hourOfDay else "" + hourOfDay
        val minuteString = if (minute < 10) "0" + minute else "" + minute
        val secondString = if (second < 10) "0" + second else "" + second
        val time = "You picked the following time: " + hourString + "h" + minuteString + "m" + secondString + "s"
//        timeTextView!!.text = time
    }

    */
}// Required empty public constructor