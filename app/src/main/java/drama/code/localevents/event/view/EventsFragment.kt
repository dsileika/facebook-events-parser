package drama.code.localevents.event.view

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Fragment
import android.content.*
import android.location.*
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import drama.code.localevents.R
import drama.code.localevents.event.model.Event
import drama.code.localevents.event.module.EventsModule
import drama.code.localevents.event.presenter.IEventPresenter
import javax.inject.Inject
import drama.code.localevents.App
import android.location.LocationListener
import android.net.ConnectivityManager
import android.os.StrictMode
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.view.*
import android.widget.*
import com.afollestad.materialdialogs.MaterialDialog
import com.facebook.AccessToken
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import dagger.Provides
import drama.code.localevents.Utils.CalUtil
import drama.code.localevents.Utils.NetworkUtil
import drama.code.localevents.event.component.EventsComponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import ru.solodovnikov.rxlocationmanager.LocationTime
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import ru.solodovnikov.rxlocationmanager.IgnoreErrorTransformer
import ru.solodovnikov.rxlocationmanager.LocationRequestBuilder
import ru.solodovnikov.rxlocationmanager.RxLocationManager
import rx.Scheduler
import rx.Single
import rx.Subscription
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by localevents on 5/11/17.
 */
class EventsFragment : Fragment(), IEventView, EventsAdapter.Callback, ActivityCompat.OnRequestPermissionsResultCallback {

    private val REQUEST_CODE_LOCATION_PERMISSIONS = 150


    // inside a basic activity
    // Create persistent LocationManager reference
    var locationManager: LocationManager? = null
    var receiver: BroadcastReceiver? = null
    var isEvents: Boolean = false



    @Inject
    lateinit var eventsPresenter: IEventPresenter
    @Inject
    lateinit var adapter: EventsAdapter

    private lateinit var progressTxt: TextView
    private lateinit var loadingLayout: ConstraintLayout

    private var checkPermissions = true

    private lateinit var activt: Activity

    private val rxLocationManager: RxLocationManager by lazy { RxLocationManager(activity.applicationContext) }
    private val locationRequestBuilder: LocationRequestBuilder by lazy { LocationRequestBuilder(activity.applicationContext) }

    var previousBestLocation: Location? = null

    var action_change_distance: MenuItem? = null
    var action_change_date: MenuItem? = null
    var action_change_filter: MenuItem? = null


    val EVENTS_FRAGMENT = "EventsFragment"

    val component by lazy {
        App.get(activity).appComponent.plus(EventsModule(this))
    }

    private var events = mutableListOf<Event>()
    private var callback: Callback? = null
    private var rootView: View? = null

    var meters: Int = 0
    var met: Int = 0
    var seekerDialog: AlertDialog? = null
    var dateDialog: AlertDialog? = null
    var calutil: CalUtil? = null

    private var startDate: Calendar = Calendar.getInstance()
    private var sinceDate: Calendar = Calendar.getInstance()
    var sinceStamp: String = toTimestamp(Calendar.getInstance()).toString()

    private var endDate: Calendar = Calendar.getInstance()
    private var untilDate: Calendar = Calendar.getInstance()
    var untilStamp: String = toTimestamp(Calendar.getInstance()).toString()


    lateinit var categorysVal: Array<String>
    lateinit var categorys: MutableList<String>
    var selectedIndex: Int = 0

    private lateinit var prefs: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    // AdMob variable
    private lateinit var mAdView : AdView




    override fun onAttach(context: Context?) {
        callback = context as Callback
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        getPrefs()
        component.inject(this)
        calutil = CalUtil()
        untilDate = calutil!!.day(1,sinceDate)
        endDate = untilDate
        untilStamp = toTimestamp(untilDate).toString()
        super.onAttach(context)
    }


    fun getPrefs(){
        this.selectedIndex = prefs.getInt("selectedIndex",0)
        this.meters = prefs.getInt("meters",100)
    }

    fun setPrefs(){
        editor = prefs.edit()
        editor.putInt("selectedIndex",selectedIndex)
        editor.putInt("meters",meters)
        editor.apply()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(receiver == null) registerBroadcastReceiver()

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
//        MobileAds.initialize(activity, resources.getString(R.string.admob_id))

        categorys = mutableListOf(resources.getString(R.string.distance), resources.getString(R.string.time), resources.getString(R.string.popularity))
        categorysVal = arrayOf("eventDistance", "time", "popularity")


        locationManager = activity.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager


        // Init alerts
        initAlert(1)
        initAlert(2)

//        endDate.add(Calendar.DATE, 1)


//        eventsPresenter.destroy()
        eventsPresenter.setView(this)
        setHasOptionsMenu(true)

    }

    override fun onStop() {
        super.onStop()
        setPrefs()
    }


    private fun initAlert(alertType: Int){
        when(alertType){
            1 -> {
                // Seeker
                val dialog = AlertDialog.Builder(activity)
                dialog.setView(R.layout.custom_seek)
                dialog.setPositiveButton(R.string.change, {_, _ -> if(met != meters) {
                    meters = met
                    // TODO: Update events
                    eventsPresenter.displayEvents(previousBestLocation!!.latitude,previousBestLocation!!.longitude, AccessToken.getCurrentAccessToken().token, met, sinceStamp, untilStamp, getCategory(selectedIndex))
                } })
                dialog.setNegativeButton(android.R.string.cancel, { _, _ ->  seekerDialog!!.dismiss() })
                seekerDialog = dialog.create()
            }
            2 -> {
                // Date
                val dialog = AlertDialog.Builder(activity)
                dialog.setView(R.layout.custom_date)
                dialog.setPositiveButton(R.string.change, {_, _ -> if(sinceDate != startDate || untilDate != endDate) {
                    sinceDate = startDate
                    untilDate = endDate
                    val tstamp = toTimestamp(startDate).toString()
                    val endtstamp = toTimestamp(endDate).toString()
                    sinceStamp = tstamp
                    untilStamp = endtstamp
                    // TODO: Update events
                    eventsPresenter.displayEvents(previousBestLocation!!.latitude,previousBestLocation!!.longitude, AccessToken.getCurrentAccessToken().token, meters, tstamp, endtstamp, getCategory(selectedIndex))
                } })
                dialog.setNegativeButton(android.R.string.cancel, {_,_ -> })
                dialog.setOnDismissListener({startDate = sinceDate; endDate = untilDate})
                dateDialog = dialog.create()
            }

        }
    }

    fun getCategory(cat: Int): String {
        return categorysVal[cat]
    }

    @SuppressLint("SimpleDateFormat")
    private fun toTimestamp(date: Calendar): Long {
        val dater = setFields("string",date)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS")
        val parsedDate = dateFormat.parse(dater)
        val timestamp: Long = parsedDate.time/1000;
        return timestamp
    }


    @SuppressLint("SetTextI18n", "CommitTransaction")
    private fun changeAlert(alertType: Int){

        when(alertType){
            1 -> {
                // Seeker
                seekerDialog!!.setTitle(R.string.set_distance);
                seekerDialog!!.show()

                if(seekerDialog!!.isShowing) {
                    val seekbar = seekerDialog!!.findViewById<View>(R.id.seekBar1) as DiscreteSeekBar
                    val textSeek = seekerDialog!!.findViewById<View>(R.id.textsneek) as TextView
                    //final TextView tv_dialog_size = (TextView) dialog.findViewById(R.id.set_size_help_text);

                    // and you can call seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    //implement methods.

                    seekbar.progress = meters
                    textSeek.text = meters.toString() + " " + resources.getString(R.string.meters)

                    seekbar.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {

                        val stepSize = 10

                        override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                            met = (value / stepSize) * stepSize;
                            seekBar.progress = met
                            textSeek.text = seekbar.progress.toString() + " " + resources.getString(R.string.meters)
                        }

                        override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                            // Do something here,
                            //if you want to do anything at the start of
                            // touching the seekbar
                        }

                        override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {

                        }
                    })
                }

            }
            2 -> {
                // Date
                dateDialog!!.setTitle(R.string.set_date);
                dateDialog!!.show()

                if(dateDialog!!.isShowing) {

                    if(App.compareDates(untilDate, sinceDate, "date") != 1){
                        untilDate = calutil!!.day(1,sinceDate)
                        untilStamp = toTimestamp(untilDate).toString()
                    }

                      // Show a datepicker when the dateButton is clicked
                    val from_date = dateDialog!!.findViewById<View>(R.id.from_date) as TextView
                    val from_time = dateDialog!!.findViewById<View>(R.id.from_time) as TextView
                    val to_date = dateDialog!!.findViewById<View>(R.id.to_date) as TextView
                    val to_time = dateDialog!!.findViewById<View>(R.id.to_time) as TextView


                    setFields("date", startDate, from_date)
                    setFields("time", startDate, from_time)

                    setFields("date", endDate, to_date)
                    setFields("time", endDate, to_time)

                    from_date.setOnClickListener({
                        getDate("startDate", from_date, startDate)
                    })

                    from_time.setOnClickListener({
                        getTime("startTime", from_time, startDate)

                    })

                    to_date.setOnClickListener({

                        getDate("endDate", to_date, endDate)
                    })

                    to_time.setOnClickListener({
                        getTime("endTime", to_time, endDate)

                    })

                }

            }
            3 -> {

                // Filter
                val dialog = MaterialDialog.Builder(activity)
                // Set the adapter
                dialog.items(categorys)

                dialog.itemsCallbackSingleChoice(selectedIndex) { dialog, itemView, which, text ->

                    /**
                     * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                     * returning false here won't allow the newly selected radio button to actually be selected.
                     **/
                    true
                }


                dialog.positiveText(R.string.change)
                dialog.negativeText(android.R.string.cancel)
                // Filter
                dialog.title(R.string.set_filter);

                dialog.onPositive { dialog, _ ->
                    if(dialog.selectedIndex != selectedIndex) {
                        selectedIndex = dialog.selectedIndex
                        eventsPresenter.displayEvents(previousBestLocation!!.latitude, previousBestLocation!!.longitude, AccessToken.getCurrentAccessToken().token, meters, sinceStamp,untilStamp, getCategory(dialog.selectedIndex))
                    }
                }

                dialog.show()
//                filterDialog = dialog.build()
//                filterDialog!!.show()


            }
        }
    }



    fun setFields(strName: String, mDater: Calendar, txtV: TextView? = null): String {

        val year = mDater.get(Calendar.YEAR)
        val month = if(mDater.get(Calendar.MONTH) < 10) "0" + (mDater.get(Calendar.MONTH)+1) else "" + (mDater.get(Calendar.MONTH)+1)
        val day = if(mDater.get(Calendar.DAY_OF_MONTH) < 10) "0" + mDater.get(Calendar.DAY_OF_MONTH) else "" + mDater.get(Calendar.DAY_OF_MONTH)

        val hour = if(mDater.get(Calendar.HOUR_OF_DAY) < 10) "0" + mDater.get(Calendar.HOUR_OF_DAY) else "" + mDater.get(Calendar.HOUR_OF_DAY)
        val minuter = if(mDater.get(Calendar.MINUTE) < 10) "0" + mDater.get(Calendar.MINUTE) else "" + mDater.get(Calendar.MINUTE)


        when(strName){
            "date" -> txtV!!.text = year.toString() + "-" + month + "-" + day
            "time" -> txtV!!.text = hour + ":" + minuter
            "string" -> return year.toString() + "-" + month + "-" + day +" "+hour + ":" + minuter +":00.0"
        }
        return ""
    }


    /**
     * GET/SET datepicker
     */

    fun getDate(strName: String, txtV: TextView, mDater: Calendar) {

        val minDate = if(strName == "endDate") calutil!!.day(1,startDate) else Calendar.getInstance()

        val dateFragmenet = DatePickerFragment().newInstance(minDate, DatePickerDialog.OnDateSetListener {
            _, year, monthOfYear, dayOfMonth ->
            val now: Calendar = Calendar.getInstance()
            val month = if(monthOfYear < 10) "0" + (monthOfYear+1) else "" + (monthOfYear+1)
            val day = if(dayOfMonth < 10) "0" + dayOfMonth else "" + dayOfMonth
            txtV.text = year.toString() + "-" + month + "-" + day

            if(App.compareDates(App.toCalendar(year, monthOfYear, dayOfMonth, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)), now, "date") == 3) {
                setDate(strName, App.toCalendar(year, monthOfYear, dayOfMonth, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)))
            }  else
                setDate(strName,App.toCalendar(year,monthOfYear,dayOfMonth, mDater.get(Calendar.HOUR_OF_DAY), mDater.get(Calendar.MINUTE)))

        }, mDater)

        dateFragmenet.show(fragmentManager, "DatePickerFragment")
    }

    private fun setDate(strName: String, data: Calendar){
        when(strName){
            "startDate" -> {
                /**
                1: Date1 is after Date2
                2: Date1 is before Date2
                3: Date1 is equal to Date2
                 */
               /* if(App.compareDates(data, endDate) == 1) {
                    increaseDayforEnd()
                }*/

                this.startDate = data
            }
            "endDate" -> {
                this.endDate = data
            }
        }

        increaseDayforEnd()

    }

    private fun increaseDayforEnd(){

        if(App.compareDates(endDate, startDate, "date") != 1){
            endDate = calutil!!.day(1,startDate)
        }

/*        if(App.compareDates(endDate, startDate, "time") != 1){
            endDate = calutil!!.hour(0,endDate,startDate)
        }*/

        val from_date = dateDialog?.findViewById<View>(R.id.from_date) as TextView
        val from_time = dateDialog?.findViewById<View>(R.id.from_time) as TextView
        val to_date = dateDialog!!.findViewById<View>(R.id.to_date) as TextView
        val to_time = dateDialog!!.findViewById<View>(R.id.to_time) as TextView

        setFields("date", startDate, from_date)
        setFields("time", startDate, from_time)

        setFields("date", endDate, to_date)
        setFields("time", endDate, to_time)
    }

    /**
     * GET/SET timepicker
     */
    fun getTime(strName: String, txtV: TextView, mTimer: Calendar){

        val minDate = if(strName == "endTime") startDate else Calendar.getInstance()
//        val minDate = Calendar.getInstance()
            val now: Calendar = Calendar.getInstance()
        val isEquals = App.compareDates(App.toCalendar(mTimer.get(Calendar.YEAR), mTimer.get(Calendar.MONTH), mTimer.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)), Calendar.getInstance(),"date") == 3

        TimePickerFragment().newInstance(minDate, TimePickerDialog.OnTimeSetListener{ _, hourOfDay, minute, _ ->
            val hour = if(hourOfDay < 10) "0" + hourOfDay else "" + hourOfDay
            val minuter = if(minute < 10) "0" + minute else "" + minute
            txtV.text = hour + ":" + minuter

//            if(App.compareDates(startDate, Calendar.getInstance(),"date") == 3)
//                setTime(strName,App.toCalendar(mTimer.get(Calendar.YEAR), mTimer.get(Calendar.MONTH), mTimer.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)))
//            else
                setTime(strName,App.toCalendar(mTimer.get(Calendar.YEAR), mTimer.get(Calendar.MONTH), mTimer.get(Calendar.DAY_OF_MONTH), hourOfDay, minute))

        }, mTimer, /*if(strName == "endTime") true else */ isEquals).show(fragmentManager, "DatePickerFragment")
    }

    private fun setTime(strName: String, data: Calendar){
        when(strName){
             "startTime" -> {
                 /**
                 1: Date1 is after Date2
                 2: Date1 is before Date2
                 3: Date1 is equal to Date2
                  */
//                 if(App.compareDates(data, endDate) == 1) {
//                     increaseDayforEnd()
//                 }
                 this.startDate = data
             }
            "endTime" -> {
                this.endDate = data
            }
        }

        increaseDayforEnd()
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater!!.inflate(R.menu.menu_main, menu)

        action_change_distance = menu!!.findItem(R.id.action_change_distance)
        action_change_date = menu.findItem(R.id.action_change_date)
        action_change_filter = menu.findItem(R.id.action_change_filter)

        super.onCreateOptionsMenu(menu,inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id: Int = item.itemId;


        //noinspection SimplifiableIfStatement

        if(id == R.id.action_change_distance){
            if(action_change_distance!!.isVisible) changeAlert(1)
            return true
        }

        if(id == R.id.action_change_date){
            if(action_change_date!!.isVisible) changeAlert(2)
            return true
        }

        if(id == R.id.action_change_filter){
            if(action_change_filter!!.isVisible) changeAlert(3)
            return true
        }

        return super.onOptionsItemSelected(item);
    }


    var rvMatches: RecyclerView? = null
    var emptyView: ConstraintLayout? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val rootView = inflater?.inflate(R.layout.fragment_matches, container, false)
        rootView?.let { initLayoutReferences(it) }!!
/*        rootView.addOnLayoutChangeListener(View.OnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            //                var i = Intent(activity, EventDetailActivity::class.java)
//                i.putExtra("event_name", oneEvent!!.getName())
//                i.putExtra("event_image", oneEvent!!.getCoverPicture())
//                i.putExtra("event_description", oneEvent!!.getDescription())
//                startActivity(i)
        })*/
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        println(savedInstanceState)
        requestBuild()
    }

    private fun initLayoutReferences(rootView: View) {
        this.rootView = rootView
        this.rvMatches = rootView.findViewById<View>(R.id.rv_matches) as RecyclerView
        this.emptyView = rootView.findViewById<View>(R.id.empty_view) as ConstraintLayout
        this.progressTxt = rootView.findViewById<View>(R.id.progressTxt) as TextView
        this.loadingLayout = rootView.findViewById<View>(R.id.loadingLayout) as ConstraintLayout
        this.mAdView = rootView.findViewById(R.id.adView)

        rvMatches!!.setHasFixedSize(true)

        rvMatches!!.layoutManager = LinearLayoutManager(activity)

        this.adapter.updateEvents(this.events)

        rvMatches!!.adapter = this.adapter

    }

    override fun showEvents(events: List<Event>) {
        this.events.clear()
        this.events.addAll(events)
        this.adapter.notifyDataSetChanged()

        if (events.isEmpty()) {
            rvMatches!!.visibility = View.GONE
            emptyView!!.visibility = View.VISIBLE
            action_change_distance?.isVisible = previousBestLocation != null
            action_change_date?.isVisible = previousBestLocation != null
            action_change_filter?.isVisible = previousBestLocation != null

            mAdView.destroy()
        }
        else {

            rvMatches!!.visibility = View.VISIBLE
            emptyView!!.visibility = View.GONE
            action_change_distance?.isVisible = previousBestLocation != null
            action_change_date?.isVisible = previousBestLocation != null
            action_change_filter?.isVisible = previousBestLocation != null

            rvMatches!!.layoutManager!!.scrollToPosition(0)

           /* val adRequest = AdRequest.Builder()
                    .addTestDevice("C2C7FBE85CD4BCFBA9640BD0821CFAA3") // Nexus 5
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR) // All emulators
                    .build()
            mAdView.loadAd(adRequest)*/
        }
    }

    fun setVisible(){
        rvMatches!!.visibility = View.VISIBLE
//        emptyView!!.visibility = View.GONE
        action_change_distance?.isVisible = true
        action_change_date?.isVisible = true
        action_change_filter?.isVisible = true

//        rvMatches!!.layoutManager.scrollToPosition(0)
    }

    fun setHiden(){
//        rvMatches!!.visibility = View.GONE
//        emptyView!!.visibility = View.VISIBLE
        action_change_distance?.isVisible = false
        action_change_date?.isVisible = false
        action_change_filter?.isVisible = false

        mAdView.destroy()
    }

    override fun loadingStarted() {
        callback!!.onCityLoaded(resources.getString(R.string.empty_city))
        loadingLayout.visibility = View.VISIBLE;
        action_change_distance?.isVisible = false
        action_change_date?.isVisible = false
        action_change_filter?.isVisible = false
        progressTxt.setText(R.string.loading_events)
//        Toast.makeText(activity, R.string.loading_events, Toast.LENGTH_SHORT).show()
    }

    override fun loadingFailed(errorMessage: String?) {
        callback?.onCityLoaded(resources.getString(R.string.empty_city))
        loadingLayout.visibility = View.GONE;
        action_change_distance?.isVisible = false
        action_change_date?.isVisible = false
        action_change_filter?.isVisible = false
        progressTxt.setText(R.string.loading_events_error)
//        Toast.makeText(activity, R.string.loading_events_error, Toast.LENGTH_SHORT).show()
    }


    private fun noRecordFound(){
        Toast.makeText(activity, R.string.cant_find_events, Toast.LENGTH_SHORT).show()
    }

    override fun loadingDone(){
        loadingLayout.visibility = View.GONE
        action_change_distance?.isVisible = false
        action_change_date?.isVisible = false
        action_change_filter?.isVisible = false
        progressTxt.text = ""
    }


    override fun onEventClicked(event: Event) {
        callback?.onEventClicked(event)
//        Toast.makeText(activity, "Sorry have no time to implement players details", Toast.LENGTH_LONG).show()
    }

    override fun openFb(fbId: String) {
        callback?.openFb(fbId)
    }




    interface Callback {
        fun onEventsLoaded(event: Event)
        fun onEventClicked(event: Event)
        fun onCityLoaded(city: String)
        fun openFb(fbId: String)
    }

    override fun onCityLoaded(city: String) {
        callback?.onCityLoaded(city)
    }

    private lateinit var requestSub: rx.Subscription



    private fun requestBuild() {

       requestSub = locationRequestBuilder.addLastLocation(LocationManager.NETWORK_PROVIDER, LocationTime(1, TimeUnit.HOURS), IgnoreErrorTransformer(null))
                .addRequestLocation(LocationManager.NETWORK_PROVIDER, LocationTime(30, TimeUnit.SECONDS), IgnoreErrorTransformer(null))
//                .addRequestLocation(LocationManager.GPS_PROVIDER, LocationTime(15, TimeUnit.SECONDS))
                .setDefaultLocation(Location(LocationManager.NETWORK_PROVIDER))
                .create()
                .subscribe({t -> showLocationMessage(t) },{})

    }



    override fun onDestroy() {
        super.onDestroy()
        eventsPresenter.destroy()
        locationManager!!.removeUpdates(locationlistener)
        if (receiver != null) {
            unregisterBroadcastReceiver()
            receiver = null;
        }
    }


    @SuppressLint("MissingPermission")
    private fun showLocationMessage(location: Location?) {
        requestSub.unsubscribe()

//        showSnackbar("$methodName Success: ${location?.toString() ?: "Empty location"}")
//        showSnackbar("Provider: $provider")
            if (location!!.latitude != 0.0 && location.longitude != 0.0 && previousBestLocation != location) {
                previousBestLocation = location
                // TODO: Update events
                eventsPresenter.displayEvents(location.latitude, location.longitude, AccessToken.getCurrentAccessToken().token, meters, sinceStamp,untilStamp, getCategory(selectedIndex))

                println(location.provider)
                when (location.provider) {
                    "network" -> {
                        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000L, 100f, locationlistener)
                    }
                    "gps" -> {
                        println("GPS")
                        locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 100f, locationlistener)
                    }
                    else -> false
                }


            }
    }

    fun isAvailableLocation(loc1: Location, loc2: Location): Boolean {
//        return loc1.longitude > 124 && loc1.longitude < 132 && loc1.latitude > 33 && loc1.latitude < 43
        return loc1.longitude.toInt() != loc2.longitude.toInt() && loc1.latitude.toInt() != loc2.latitude.toInt()
    }

    fun distFrom(loc1: Location, loc2: Location): Int {

        val lat1 = loc1.latitude
        val lng1 = loc1.longitude

        val lat2 = loc2.latitude
        val lng2 = loc2.longitude

        val earthRadius = 6371000.0 //meters
        val dLat = Math.toRadians((lat2 - lat1))
        val dLng = Math.toRadians((lng2 - lng1))
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                Math.sin(dLng / 2) * Math.sin(dLng / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        return (earthRadius * c).toInt()
    }


    private fun showErrorMessage() {
        rvMatches!!.visibility = View.GONE
        emptyView!!.visibility = View.VISIBLE
        action_change_distance?.isVisible = false
        action_change_date?.isVisible = false
        action_change_filter?.isVisible = false
//        showSnackbar("$methodName Error: ${throwable.message}")
    }



    private val locationlistener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            if(distFrom(location, previousBestLocation!!) >= 100) {
//                 TODO: Update events
                previousBestLocation = location
                eventsPresenter.displayEvents(location.latitude,location.longitude, AccessToken.getCurrentAccessToken().token, meters, sinceStamp,untilStamp, getCategory(selectedIndex))
            }
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        }
        override fun onProviderEnabled(provider: String) {
        }
        override fun onProviderDisabled(provider: String) {
        }
    }

    /**
     * This method enables the Broadcast receiver for
     * "android.intent.action.TIME_TICK" intent. This intent get
     * broadcasted every minute.
     *
     * @param view
     */
    private fun registerBroadcastReceiver() {

        // [START]
        // your oncreate code should be

        val filter = IntentFilter()
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        // Add this inside your class
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                // [networkonmainthreadexception] (Start)
                // Bug fix: https://stackoverflow.com/questions/5150637/networkonmainthreadexception
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)
                // [networkonmainthreadexception] (End)

//        Log.d("app", "Network connectivity change")

                if (NetworkUtil.hasActiveInternetConnection(activity)) {
                    // Done
//                    if(isInit == false) {
//                        isInit = true
                    setVisible()
                  if(previousBestLocation != null)  eventsPresenter.displayEvents(previousBestLocation!!.latitude, previousBestLocation!!.longitude, AccessToken.getCurrentAccessToken().token, meters, sinceStamp,untilStamp, getCategory(selectedIndex))
//                    }
                } else {
                    setHiden()
                }
                if (intent.extras!!.getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, java.lang.Boolean.FALSE)) {
//            Log.d("app", "There's no network connectivity")
//                    isInit = false
                    setHiden()
                    Toast.makeText(activity, R.string.lost_connection, Toast.LENGTH_LONG).show()
                }
            }

        }

        activity.registerReceiver(receiver, filter)

        // [END]

    }

    /**
     * This method disables the Broadcast receiver
     *
     * @param view
     */
    private fun unregisterBroadcastReceiver() {
        activity.unregisterReceiver(receiver)
    }

    override fun onResume() {
        if(receiver == null) registerBroadcastReceiver()
        super.onResume()
    }


}










