package drama.code.localevents.event.presenter

import drama.code.localevents.event.model.Events
import drama.code.localevents.event.view.IEventView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by localevents on 5/11/17.
 */
class EventPresenter(eventsEndPoint: IEventsEndPoint) : IEventPresenter {


//    @Inject
    private var view: IEventView? = null
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
//    @Inject
    var eventsEndpoint: IEventsEndPoint = eventsEndPoint

    private var disposableMatches: Disposable? = null

    override fun displayEvents(lat: Double, lng: Double, accesstoken: String, meters: Int, since: String, until: String,sort: String) {

        disposableMatches?.dispose()

        disposableMatches = eventsEndpoint.fetchEvents(lat.toString(), lng.toString(), accesstoken, meters, since, until, sort)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe({
                    if (isViewAttached()) {
                        view!!.loadingStarted()
                    }
                    }).subscribeBy(
                    onNext = { events ->
                        disposableMatches!!.dispose()
                        onEventsResult(events)
                    },
                    onError = { error ->
                        onEventsFetchError(error)
                    },
                    onComplete = {

                    })

//        disposableMatches.let { compositeDisposable.add(it!!) }

    }

    override fun setView(view: IEventView) {
        this.view = view
    }

    override fun destroy() {
        this.view = null
        compositeDisposable.clear()
        disposableMatches?.dispose()
    }


    private fun onEventsResult(events: Events) {

        val _events = events.getEvents()

        if (isViewAttached()) {
            if (_events != null) {
                view!!.loadingDone()
                view!!.showEvents(_events)
            }
        }

    }

    private fun onEventsFetchError(throwable: Throwable) {
        if (isViewAttached()) {
            view!!.loadingFailed(throwable.message)
        } else {
            // do nothing
        }
    }

    private fun isViewAttached(): Boolean {
        return view != null
    }
}