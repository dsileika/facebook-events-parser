package drama.code.localevents.event.model

/**
 * Created by localevents on 5/11/17.
 */

class Events {

    private var events: List<Event>? = null

    fun getEvents(): List<Event>? {
        return events
    }

    fun setEvents(events: List<Event>) {
        this.events = events
    }



}