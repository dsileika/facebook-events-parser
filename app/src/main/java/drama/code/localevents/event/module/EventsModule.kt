package drama.code.localevents.event.module

import drama.code.localevents.event.presenter.IEventPresenter
import drama.code.localevents.event.presenter.IEventsEndPoint
import drama.code.localevents.event.presenter.EventPresenter
import drama.code.localevents.event.view.EventsAdapter
import drama.code.localevents.event.view.EventsFragment

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by localevents on 6/11/17.
 */
@Module
class EventsModule(private val fragment: EventsFragment) {

    @Provides
    @FragmentScope
    fun provideEventsFragment(): EventsFragment {
        return fragment
    }

    @Provides
    @FragmentScope
    fun provideEventsPresenter(eventsEndPoint: IEventsEndPoint): IEventPresenter {
        return EventPresenter(eventsEndPoint)
    }

    @Provides
    @FragmentScope
    fun provideEventsEndpoint(retrofit: Retrofit): IEventsEndPoint {
        return retrofit.create(IEventsEndPoint::class.java)
    }

    @Provides
    @FragmentScope
    fun provideEventsAdaptor(): EventsAdapter {
        return EventsAdapter(fragment)
    }

}