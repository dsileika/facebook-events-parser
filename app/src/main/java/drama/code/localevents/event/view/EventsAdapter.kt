package drama.code.localevents.event.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import drama.code.localevents.R
import drama.code.localevents.event.model.Event
import kotlinx.android.synthetic.main.match_item.view.*
import drama.code.localevents.Utils.LinkUtils
import drama.code.localevents.App

/**
 * Created by localevents on 5/11/17.
 */
class EventsAdapter(callback: Callback) : RecyclerView.Adapter<EventsAdapter.ItemMatchViewHolder>() {

    private var mcallback: Callback? = null
    var events: MutableList<Event>? = null
    var context: Context? = null


    init {
        mcallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMatchViewHolder {
        context = parent?.context
        val layoutInflater = LayoutInflater.from(parent?.context)

        return ItemMatchViewHolder(layoutInflater.inflate(R.layout.match_item, parent, false))
    }
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ItemMatchViewHolder, position: Int) {


        val event = events?.get(position)

        if(!events!![position].getPlace()?.getCity().isNullOrEmpty())
        mcallback?.onCityLoaded(events!![position].getPlace()?.getCity()!!)
            else
        mcallback?.onCityLoaded(context!!.getString(R.string.empty_city))


//            holder?.matchStatType?.text = events?.get(position)?.getName()

            /*
        val playerAPicURL = API.PLAYER_PIC_URL.value.replace("@playerId",
                matches?.get(position)?.getTeamA()?.getTopPlayers()?.get(position)?.getId().toString())
        */

        if(!event?.getVenue()?.getStreet().isNullOrEmpty()) {
            holder?.locationInMap?.visibility = View.VISIBLE
            holder?.locationInMap?.text = event?.getVenue()?.getStreet()
            holder?.locationInMap?.setOnClickListener {
                if (this.context != null && event?.getVenue()?.getLat() != null && event.getVenue()?.getLng() != null && event.getVenue()?.getStreet() != null) {
                    App.openInMap(this.context!!, event.getVenue()?.getLat()!!, event.getVenue()?.getLng()!!, event.getVenue()?.getStreet()!!)
                }
            }
        } else {
            holder?.locationInMap?.visibility = View.GONE
        }
//        holder?.locationInMap?.movementMethod = LinkMovementMethod.getInstance()


        val playerAPicURL = events?.get(position)?.getCoverPicture()

            Glide.with(context).load(playerAPicURL)
                    .placeholder(R.drawable.headshot_blank_large)
                    .error(R.drawable.headshot_blank_large)
                    .into(holder?.matchPalyerAImage)

            holder?.matchPalyerAShortName?.text = event?.getVenue()?.getName()


            holder?.openFB?.setOnClickListener {
                mcallback?.openFb(event?.getId().toString())
            }

//            holder?.matchPalyerAJumperNumber?.text = events?.get(position)?.getDescription()
//            holder?.matchPalyerAPosition?.text = events?.get(position)?.getDescription()


            /*
        val playerBPicURL = API.PLAYER_PIC_URL.value.replace("@playerId",
                matches?.get(position)?.getTeamB()?.getTopPlayers()?.get(position)?.getId().toString())


        Glide.with(context).load(playerBPicURL)
                .placeholder(R.drawable.headshot_blank_large)
                .error(R.drawable.headshot_blank_large)
                .into(holder?.matchPalyerBImage)

        holder?.matchPalyerBShortName?.text = matches?.get(position)?.getTeamB()?.getTopPlayers()?.get(position)?.getShortName()
        holder?.matchPalyerBJumperNumber?.text = matches?.get(position)?.getTeamB()?.getTopPlayers()?.get(position)?.getJumperNumber().toString()
        holder?.matchPalyerBPosition?.text = matches?.get(position)?.getTeamB()?.getTopPlayers()?.get(position)?.getPosition()
        */
            holder?.itemView?.setOnClickListener {
                if (event != null) {
                    mcallback?.onEventClicked(event)
                }
            }
        if(!event?.getStartTime().isNullOrEmpty()) {
            holder?.eventtime?.visibility = View.VISIBLE
            holder?.eventtime?.text = event?.getStartTime() + event?.getEndTime()
        } else {
            holder?.eventtime?.visibility = View.GONE
        }

        if(event?.eventDistance() != null){
            holder?.eventdistancer!!.visibility = View.VISIBLE
            holder.eventdistancer!!.text = if(event.eventDistance()!! < 1000) event.eventDistance().toString() +" "+ context!!.getString(R.string.meters) else (event.eventDistance()!! * 0.001).toString() +" "+ context!!.getString(R.string.kilometers)
        } else {
            holder?.eventdistancer!!.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        if(events != null) {
            return events?.size!!
        } else
            return 1
    }

    fun updateEvents(events: MutableList<Event>?) {
        this.events = events
    }

    interface Callback {
        fun onEventClicked(event: Event)
        fun onCityLoaded(city: String)
        fun openFb(fbId: String)
    }


    class ItemMatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {

//        var matchStatType = view.tv_statType

        var matchPalyerAImage = view.person_photo
        var matchPalyerAShortName = view.person_name
        var eventdistancer = view.eventdistance
        var eventtime = view.eventtime
        var locationInMap = view.locationinmap
        var openFB = view.fbEvent
//        var matchPalyerAPosition = view.tv_palyerAPosition

//        var matchPalyerBImage = view.iv_palyerB
//        var matchPalyerBShortName = view.tv_palyerBShortName
//        var matchPalyerBJumperNumber = view.tv_palyerBJumperNumber
//        var matchPalyerBPosition = view.tv_palyerBPosition
    }

}
