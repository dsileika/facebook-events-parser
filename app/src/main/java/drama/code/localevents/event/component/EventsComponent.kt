package drama.code.localevents.event.component

import drama.code.localevents.event.module.EventsModule
import drama.code.localevents.event.module.FragmentScope
import drama.code.localevents.event.view.EventsFragment
import dagger.Subcomponent

/**
 * Created by localevents on 6/11/17.
 */
@FragmentScope
@Subcomponent(modules = arrayOf(EventsModule::class))
interface EventsComponent {
    fun inject(eventsFragment: EventsFragment)
}