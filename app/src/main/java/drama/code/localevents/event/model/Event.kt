package drama.code.localevents.event.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import drama.code.localevents.App

/**
 * Created by dofke on 2018-01-01.
 */
class Event {

    @SerializedName("id")
    @Expose
    private var id: String? = null

    @SerializedName("description")
    @Expose
    private var description: String? = null

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("about")
    @Expose
    private var about: String? = null

    @SerializedName("emails")
    @Expose
    private var emails: Array<String>? = null

    @SerializedName("coverPicture")
    @Expose
    private var coverPicture: String? = null

    @SerializedName("profilePicture")
    @Expose
    private var profilePicture: String? = null

    @SerializedName("category")
    @Expose
    private var category: String? = null

    @SerializedName("categoryList")
    @Expose
    private var categoryList: Array<String>? = null

    @SerializedName("startTime")
    @Expose
    private var startTime: String? = null

    @SerializedName("endTime")
    @Expose
    private var endTime: String? = null


    @SerializedName("location")
    var mLocation: Location? = null

    @SerializedName("distances")
    var mDistance: Distance? = null

    @SerializedName("venue")
    @Expose
    private var venue: Event? = null

    fun getVenue(): Event? {
        return venue
    }

    @SerializedName("place")
    @Expose
    private var place: Event? = null

    fun getPlace(): Event? {
        return place
    }

    @SerializedName("metadata")
    @Expose
    private var metadata: Event? = null

    fun getLat(): Double? {
        return mLocation?.latitude
    }

    fun eventDistance(): Int? {
        return mDistance?.event
    }

    fun getLng(): Double? {
        return mLocation?.longitude
    }

    fun getStreet(): String? {
        return mLocation?.street
    }

    fun getCity(): String? {
        return mLocation?.city
    }

    fun getMetadata(): Event? {
        return metadata
    }



    fun setVenue(venue: Event){
        this.venue = venue
    }



    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }


    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getAbout(): String? {
        return about
    }

    fun setAbout(about: String) {
        this.about = about
    }

    fun getCoverPicture(): String? {
        return coverPicture
    }

    fun setCoverPicture(coverPicture: String) {
        this.coverPicture = coverPicture
    }

    fun getProfilePicture(): String? {
        return profilePicture
    }

    fun setProfilePicture(profilePicture: String?) {
        this.profilePicture = profilePicture
    }


    fun getStartTime(): String {
        if(startTime != null && startTime!!.isNotEmpty()) {
            return App.returnDataString(startTime!!, 0)
        } else {
            return ""
        }
    }

    fun getEndTime(): String {
        if(endTime != null && endTime!!.isNotEmpty() && startTime != null && startTime!!.isNotEmpty()) {
            return App.returnDataString(endTime!!, 1)
        } else {
            return ""
        }
    }


}