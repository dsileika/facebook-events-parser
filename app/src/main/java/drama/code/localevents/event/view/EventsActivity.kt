package drama.code.localevents.event.view

import android.Manifest
import android.content.*
import android.support.v7.app.AppCompatActivity
import drama.code.localevents.R
import drama.code.localevents.Utils.NetworkUtil
import drama.code.localevents.event.model.Event
import android.location.LocationManager
import android.support.v7.app.AlertDialog
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.*
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.facebook.AccessToken
import drama.code.localevents.App

import io.reactivex.Maybe
import io.reactivex.Single
import ru.solodovnikov.rxlocationmanager.RxLocationManager
import android.R.attr.fragment
import android.app.Activity
import drama.code.localevents.event.module.EventsModule


/**
 * Created by localevents on 6/11/17.
 */
class EventsActivity : AppCompatActivity(), EventsFragment.Callback {

    val EVENTS_FRAGMENT = "EventsFragment"
    var manager: LocationManager? = null
    var isInit: Boolean = false
    var firsCheck: Boolean = false
    var isShowDialog: Boolean = false
    var initDialog: Boolean = false

    private val REQUEST_CODE_LOCATION_PERMISSIONS = 150

    private var rxLocationManager: RxLocationManager? = null
//    private var locationRequestBuilder: LocationRequestBuilder? = null

    private var checkPermissions: Boolean = true
    private var isCheckDialog: Boolean = false

    var previousBestLocation: Location? = null
    private var alertDialog: AlertDialog? = null
    private var flag: Boolean = false

    val ARG_USE_EXPANSION = "arg_use_expansion"
    val ARG_EXPANSION_LEFT_OFFSET = "arg_left_offset"
    val ARG_EXPANSION_TOP_OFFSET = "arg_top_offset"
    val ARG_EXPANSION_VIEW_WIDTH = "arg_view_width"
    val ARG_EXPANSION_VIEW_HEIGHT = "arg_view_height"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Handler().postDelayed({
            if(AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired && NetworkUtil.hasActiveInternetConnection(applicationContext)) {
                val token = AccessToken.getCurrentAccessToken()
                App.fb_token = token.token
            } else {

            }
        }, 6000)

        //                finish()
        if (intent.extras!!.getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, java.lang.Boolean.FALSE)) {
//            Log.d("app", "There's no network connectivity")
//                    isInit = false
            Toast.makeText(this, R.string.lost_connection, Toast.LENGTH_LONG).show()
            isInit = false
        }


        manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if(alertDialog == null) alertDialog = AlertDialog.Builder(this).create()

        if (NetworkUtil.getConnectivityStatus(applicationContext) == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
//            Toast.makeText(applicationContext, R.string.lost_connection, Toast.LENGTH_LONG).show()
            isInit = false
        }  else {
            if (!manager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                buildAlertMessageNoGps()
                isInit = false
            } else {
                checkLocationPermission()
            }
        }

        rxLocationManager = RxLocationManager(this)
//        locationRequestBuilder = LocationRequestBuilder(rxLocationManager!!)


//        requestLocation()

//        requestBuild()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION_PERMISSIONS -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()){
                    if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        flag = false

                        // permission was granted, yay! Do the
                        // location-related task you need to do.
                        if (ActivityCompat.checkSelfPermission(this,
                                        Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {

                            //Request location updates: OKY
//                        rxLocationManager!!.onRequestPermissionsResult(permissions, grantResults)

                            if (!manager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                                buildAlertMessageNoGps()
                                isInit = false
                            } else {
                                if (!isShowDialog) {
                                    isInit = true
                                    init()
                                }
                            }

                        }
                    }
                    if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED){
                        flag = true
                        NoAccessGiven()
//                        Toast.makeText(applicationContext, "Reopen app and allow permission.",Toast.LENGTH_LONG).show()
                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }

                }
            }
            else -> {
            }
        }
    }

    private fun NoAccessGiven(){
        if(flag){
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_CODE_LOCATION_PERMISSIONS)
            isInit = false
        }
    }

/*
    isCheckDialog = false
    Log.d("VA", "Nera permission")
    checkLocationPermission()
*/

    private fun checkLocationPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_LOCATION_PERMISSIONS);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_LOCATION_PERMISSIONS);

            }
            return false;
        } else {
            if (!manager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                buildAlertMessageNoGps()
                isInit = false
            } else {
                if(!isShowDialog){
//                    isInit = true
                    init()
                }
            }
            checkPermissions = false
            return true
        }
    }







    private fun showSnackbar(text: CharSequence) {
        Toast.makeText(applicationContext, text.toString(), Toast.LENGTH_LONG).show()
    }

    private fun testSubscribe(maybe: Maybe<Location>, methodName: String) {
        maybe.subscribe({ t -> showLocationMessage(t, methodName) }, { throwable -> showErrorMessage(throwable, methodName) }) {
            val pattern = "%s Completed:"
            showSnackbar(String.format(pattern, methodName))
        }
    }

    private fun testSubscribe(single: Single<Location>, methodName: String) {
        single.subscribe({ t -> showLocationMessage(t, methodName) }) { throwable -> showErrorMessage(throwable, methodName) }
    }

    private fun showLocationMessage(location: Location?, methodName: String) {
        if(previousBestLocation != location) {
            val pattern = "%s Success: %s"
            showSnackbar(String.format(pattern, methodName, location?.toString() ?: "Empty location"))
            previousBestLocation = location
        }
    }

    private fun showErrorMessage(throwable: Throwable, methodName: String) {
        val pattern = "%s Error: %s"
        showSnackbar(String.format(pattern, methodName, throwable.message))
    }



    private fun buildAlertMessageNoGps() {
        if(!initDialog) {
            alertDialog!!.setMessage(resources.getString(R.string.check_location))
            alertDialog!!.setCancelable(false)
//            alertDialog.setButton(R.string.try_again, DialogInterface.OnClickListener { _, _ -> })
            alertDialog!!.setButton(DialogInterface.BUTTON_POSITIVE, resources.getString(R.string.try_again)) { _, _ -> }
            alertDialog!!.setOnDismissListener {
                if ((!manager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
                    alertDialog?.show()
                } else {
                    isShowDialog = false
                    checkLocationPermission()
                }
            }
            alertDialog!!.show()
            isShowDialog = true
//            isInit = true
        } else {
            if(!isShowDialog){
                isShowDialog = true
                alertDialog!!.show()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }



    private fun init(){

        if(!isInit) { // TODO: isInit - there wasn't set and have issue with twice url request
            isInit = true
            supportActionBar!!.subtitle = resources.getString(R.string.empty_city)

            val fragment = EventsFragment()
            val manager = fragmentManager
            val transaction = manager.beginTransaction()
            transaction.replace(R.id.matches_container, fragment) // newInstance() is a static factory method.
            transaction.commit()
        }
    }

    override fun onEventsLoaded(event: Event) {

    }

    override fun onCityLoaded(city: String){
        changeSubtitle(city)
    }

    override fun openFb(fbId: String){
        val intent: Intent = App.getFBIntent(this, fbId);
        startActivity(intent)
    }
    override fun onEventClicked(event: Event) {

//        loadEventDetailsFragment()
        var i = Intent(this, EventDetailActivity::class.java)

        if(!event.getId().toString().isEmpty()){
            i.putExtra("event_id", event.getId().toString())
        }

        if (!event.getVenue()?.getStreet().isNullOrEmpty()) {
            i.putExtra("street", event.getVenue()?.getStreet())
            if (event.getVenue()?.getLat() != null && event.getVenue()?.getLng() != null && event.getVenue()?.getStreet() != null) {
                i.putExtra("lat", event.getVenue()?.getLat()!!)
                i.putExtra("lng", event.getVenue()?.getLng()!!)
            }
        }
        if (event.getStartTime().isNotEmpty()) {
            i.putExtra("start_time", event.getStartTime())
            if (event.getEndTime().isNotEmpty()) i.putExtra("end_time", event.getEndTime())
        }

        if (event.eventDistance() != null) {
            i.putExtra("event_distance", event.eventDistance()!!)
        }
        i.putExtra("name", event.getVenue()?.getName())
        i.putExtra("event_name", event.getName())
        i.putExtra("event_image", event.getCoverPicture())
        i.putExtra("event_description", event.getDescription())
        startActivity(i)
    }


    private fun changeSubtitle(city: String){
        supportActionBar!!.subtitle = city
    }






/*     fun addExpansionArgs(intent: Intent): Intent {
            intent.putExtra(ARG_USE_EXPANSION, true);

            val expansionView: View = findViewById(R.id.expansion_view);

            var location: Any = Int[2];
            expansionView.getLocationInWindow(location);

            intent.putExtra(ARG_EXPANSION_LEFT_OFFSET, location[0]);
            intent.putExtra(ARG_EXPANSION_TOP_OFFSET, location[1]);
            intent.putExtra(ARG_EXPANSION_VIEW_WIDTH, expansionView.getWidth());
            intent.putExtra(ARG_EXPANSION_VIEW_HEIGHT, expansionView.getHeight());

        return intent;
    }*/



}