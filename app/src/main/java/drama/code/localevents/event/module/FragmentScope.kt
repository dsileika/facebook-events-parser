package drama.code.localevents.event.module

import javax.inject.Scope

/**
 * Created by localevents on 6/11/17.
 */
@Scope
annotation class FragmentScope