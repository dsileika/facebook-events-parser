package drama.code.localevents.event.view

import android.app.DialogFragment
import android.app.Fragment
import android.os.Bundle


import java.util.*
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog


/**
 * A simple [Fragment] subclass.
 */
class DatePickerFragment : DialogFragment() {

    private var onDateSetListener: DatePickerDialog.OnDateSetListener? = null
    private var tpd: DatePickerDialog? = null
    var mDate: Calendar? = null
    var minDate: Calendar = Calendar.getInstance()
    var isEndDate: Boolean = false

    fun newInstance(minDate: Calendar, onDateSetListener: DatePickerDialog.OnDateSetListener, mDater: Calendar?): DatePickerFragment {
        val pickerFragment = DatePickerFragment()
        pickerFragment.mDate = mDater ?: Calendar.getInstance()
        pickerFragment.minDate = minDate

        pickerFragment.setOnDateSetListener(onDateSetListener)

        //Pass the date in a bundle.
        val bundle = Bundle()
//        bundle.putSerializable(MOVE_IN_DATE_KEY, date)
//        pickerFragment.arguments = bundle
        return pickerFragment
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        super.onCreateView(inflater, container, savedInstanceState)
        /*
                It is recommended to always create a new instance whenever you need to show a Dialog.
                The sample app is reusing them because it is useful when looking for regressions
                during testing
                 */
        if (tpd == null) {
            tpd = DatePickerDialog.newInstance(
                    onDateSetListener,
                    mDate!!.get(Calendar.YEAR), mDate!!.get(Calendar.MONTH), mDate!!.get(Calendar.DAY_OF_MONTH)
            )
        } else {
            tpd!!.initialize(
                    onDateSetListener,
                    mDate!!.get(Calendar.YEAR), mDate!!.get(Calendar.MONTH), mDate!!.get(Calendar.DAY_OF_MONTH)
            )
        }

        tpd!!.setOnDismissListener { this.dismiss() }

//        if(isEndDate) minDate.set(minDate.get(Calendar.YEAR),minDate.get(Calendar.MONTH), minDate.get(Calendar.DAY_OF_MONTH) + 1, minDate.get(Calendar.HOUR), minDate.get(Calendar.MINUTE), minDate.get(Calendar.SECOND))

         tpd!!.minDate = minDate
//        tpd!!.setOnCancelListener({ Log.d("TimePicker", "Dialog was cancelled") })

        tpd!!.show(fragmentManager, "DatePicker")

        return view
    }

    /*
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        super.onCreateDialog(savedInstanceState)
        val now: Calendar = Calendar.getInstance()

//        val initialDate = arguments.getSerializable(MOVE_IN_DATE_KEY) as Date
//        val yearMonthDay = ymdTripleFor(initialDate)
        return DatePickerDialog(activity, onDateSetListener, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH))
    }
    */

    private fun setOnDateSetListener(listener: DatePickerDialog.OnDateSetListener) {
        this.onDateSetListener = listener
    }

    private fun ymdTripleFor(date: Date): IntArray {
        val cal = Calendar.getInstance(Locale.US)
        cal.time = date
        return intArrayOf(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
    }

}// Required empty public constructor