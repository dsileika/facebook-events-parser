package drama.code.localevents.event.view

import drama.code.localevents.event.model.Event
/**
 * Created by localevents on 5/11/17.
 */
interface IEventView {

    fun showEvents(events: List<Event>)

    fun loadingStarted()

    fun loadingFailed(errorMessage: String?)

    fun loadingDone()

    fun onEventClicked(event: Event)

    fun openFb(fbId: String)
}