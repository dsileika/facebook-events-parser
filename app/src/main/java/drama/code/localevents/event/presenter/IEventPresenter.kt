package drama.code.localevents.event.presenter

import drama.code.localevents.event.view.IEventView

/**
 * Created by localevents on 5/11/17.
 */
interface IEventPresenter {

    fun displayEvents(lat: Double, lng: Double, accesstoken: String, meters: Int, since: String, until: String,sort: String)

    fun setView(view: IEventView)

    fun destroy()
}