package drama.code.localevents.event.presenter

import drama.code.localevents.event.model.Events
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url
import java.util.*

/**
 * Created by localevents on 5/11/17.
 */
interface IEventsEndPoint {

//    @GET("/events/")
//    fun fetchEvents(@Url url: String): Flowable<Events>

    @GET("/events/")
    fun fetchEvents(
            @Query("lat") lat: String,
            @Query("lng") lng: String,
            @Query("accessToken") accesstoken: String,
            @Query("distance") meters: Int,
            @Query("since") since: String?,
            @Query("until") until: String?,
            @Query("sort") sort: String? = "distance"
//            @Query("since") since: String? = (System.currentTimeMillis() / 1000).toString(),
//            @Query("sort") sort: List<String>? = Arrays.asList("eventDistance")
    ): Flowable<Events>
}