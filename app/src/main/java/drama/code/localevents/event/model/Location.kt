package drama.code.localevents.event.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by dofke on 2018-01-05.
 */
class Location {

    // Location section [Start]
    @SerializedName("city")
    @Expose
    var city: String? = null

    @SerializedName("country")
    @Expose
    var country: String? = null

    @SerializedName("latitude")
    @Expose
    var latitude: Double = 0.0

    @SerializedName("longitude")
    @Expose
    var longitude: Double = 0.0

    @SerializedName("street")
    @Expose
    var street: String? = null

    @SerializedName("zip")
    @Expose
    var zip: String? = null
    // Location section [End]

}