package drama.code.localevents.event.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by dofke on 2018-01-17.
 */
class Distance {

    @SerializedName("venue")
    @Expose
    var venue: Int? = null

    @SerializedName("event")
    @Expose
    var event: Int? = null

}