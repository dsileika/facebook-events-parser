package drama.code.localevents.components

import drama.code.localevents.event.component.EventsComponent
import drama.code.localevents.event.module.EventsModule
import drama.code.localevents.modules.AppModule
import drama.code.localevents.modules.NetworkModule
import dagger.Component
import dagger.Provides
import drama.code.localevents.App
import javax.inject.Singleton

/**
 * Created by localevents on 4/11/17.
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {
    fun inject(app: App)
    fun plus(eventsModule: EventsModule): EventsComponent
}