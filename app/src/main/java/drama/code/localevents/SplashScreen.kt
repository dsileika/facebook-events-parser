package drama.code.localevents

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.facebook.login.LoginResult
import android.content.Intent
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.facebook.*
import drama.code.localevents.event.view.EventsActivity
import com.facebook.login.widget.LoginButton
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_base.*
import java.util.*
import com.facebook.FacebookException
import com.facebook.FacebookCallback
import com.facebook.login.LoginManager






/**
 * Created by dofke on 2018-01-14.
 */
class SplashScreen : AppCompatActivity() {

    var callbackManager: CallbackManager? = null

    val SPLASH_TIME_OUT: Long = 1000

    var fbBtn: LoginButton? = null
    var loginButton: Button? = null
    var bgImage: ImageView? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        loginButton = findViewById<View>(R.id.fb) as Button
        bgImage = findViewById<View>(R.id.bg) as ImageView
        fbBtn = findViewById<View>(R.id.login_button) as LoginButton

        callbackManager = CallbackManager.Factory.create()

        fbBtn!!.setReadPermissions(Arrays.asList("email", "public_profile", "user_events"))

        // Callback registration
        fbBtn!!.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

            override fun onSuccess(loginResult: LoginResult) {

                loginButton!!.visibility = View.GONE
                bgImage!!.visibility = View.GONE
//                updateWithToken()
            }

            override fun onCancel() {
                // App code
                bgImage!!.visibility = View.VISIBLE
                loginButton!!.visibility = View.VISIBLE
            }

            override fun onError(exception: FacebookException) {
                // App code
                bgImage!!.visibility = View.VISIBLE
                loginButton!!.visibility = View.VISIBLE
            }
        })

        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {

                        loginButton!!.visibility = View.GONE
                        bgImage!!.visibility = View.GONE
                        updateWithToken()
                    }

                    override fun onCancel() {
                        // App code
                        bgImage!!.visibility = View.VISIBLE
                        loginButton!!.visibility = View.VISIBLE
                    }

                    override fun onError(exception: FacebookException) {
                        // App code
                        bgImage!!.visibility = View.VISIBLE
                        loginButton!!.visibility = View.VISIBLE
                    }
                })
//
        loginButton!!.text = fbBtn!!.text

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_events"));
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired

//        if(isLoggedIn) {
//            loginButton!!.visibility = View.GONE
//            bgImage!!.visibility = View.GONE
//            updateWithToken()
//        } else {
////            bgImage!!.visibility = View.VISIBLE
////            loginButton!!.visibility = View.VISIBLE
//        }

    }

    fun onClick(v: View) {
        if (v === fb) {
//            fbBtn!!.performClick()
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_events"));
        }
    }

    fun updateWithToken() {
            Handler().postDelayed({
                val i = Intent(this@SplashScreen, EventsActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

}


